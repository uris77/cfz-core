class OutletBuilder(object):
    def __init__(self, outlet):
        self.outlet = outlet

    def add_address(self, address):
        self.outlet.address = address
        return self.outlet

    def add_manager(self, manager):
        self.outlet.managers.append(manager)
        return self.outlet

class GoodsCategory(object):
    '''A classifier for goods. This is basically
    and Enum that is persisted.
    '''
    def __init__(self, name):
        self.name = name

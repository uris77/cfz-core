from cfzcore.vehicle_pass import VehiclePass

from datetime import datetime


def create_vehicle_pass(repository, **kwargs):
    kwargs['date_issued'] = datetime.strptime(kwargs['date_issued'], '%d/%m/%Y')
    kwargs['company'] = repository.find_company_by_id(kwargs['company_id'])
    vehicle_pass = VehiclePass(**kwargs)
    vehicle_pass = repository.persist(vehicle_pass)
    return vehicle_pass.__tuple__


def fetch_all_vehicle_passes(repository):
    return repository.all()


def fetch_all_vehicle_passes_for_company(repository, company_id):
    company = repository.find_company_by_id(company_id)
    return repository.all_for_company(company)


def search_for_vehicle_pass_by_owner(owner_name, repository):
    return repository.search_by_owner(owner_name)

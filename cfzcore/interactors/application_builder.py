from cfzcore.application import Application, Status
from cfzcore.person import Person
from cfzcore.company import Company, CompanyParams


class ApplicationFormBuilder(object):

    def __init__(self, repository):
        self.repository = repository

    def create_application(self, **kwargs):
        application = self.initialize_application_form(**kwargs)
        self.repository.persist(application)
        return application.__tuple__

    def initialize_application_form(self, **kwargs):
        company_params = CompanyParams(name=kwargs['name'], 
                                       registration_number=kwargs['registration_number'])
                                       #company_type=kwargs['company_type'])
        company = Company(company_params)
        application = Application(**dict(company=company,
                                         status=Status.NEW,
                                         submission_date=kwargs['submission_date']))
        return application

    def update_basic_information_for(self, application, **kwargs):
        company = application.company
        status = kwargs.get("status")
        if status and status != 'New':
            self.validate_form_is_complete(application)
            application.status = status
        company.update_general_information(**kwargs)

    def validate_form_is_complete(self, application):
        company = application.company
        errors_exist = False
        error = ValidateErrors()
        if getattr(company, 'telephone_number', None) is None:
            errors_exist = True
            error.telephone_number = "Telephone Number: Must provide a value"
        if application.is_place_of_origin_present() is False:
            errors_exist = True
            error.place_of_origin = "Place of Origin: Can not be empty!"
        if errors_exist:
            raise FormIncompleteException("Form was incomplete", error)
        return True

    def save_consultant_for(self, application_id, **kwargs):
        application = self.repository.get(application_id)
        _consultant = getattr(application, 'consultant', None)
        if _consultant:
            consultant = application.consultant
            consultant.update_name(**kwargs)
        else:
            consultant = Person(kwargs['first_name'], kwargs['last_name'])
            application.consultant = consultant
        self.repository.persist(application)
        return application.consultant.__tuple__


class FormIncompleteException(Exception):
    def __init__(self, message, error):
        Exception.__init__(self, message)
        self.error = error

    def str(self):
        return repr(self.message)


class ValidateErrors(object):
    telephone_number = None
    place_of_origin = None

    def __init__(self):
        pass


class PendingApplicationInteractor(object):
    '''Interactor for pending applications.
    '''
    def __init__(self, repository):
        self.repository = repository

    def all_applications(self):
        '''Obtain list of all pending applications.
        '''
        return self.repository.all_with_status_pending()

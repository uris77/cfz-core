from cfzcore import enum


class AccessLetter(object):
    '''
    Access Letter for visitors to the Free Zone.
    Properties:
    granted_to
    vehicle_license_plate
    expiration_date
    date_issued
    processing_fee (paid/waived)
    receipt_id
    authorized_by (ceo/chairman)
    issued_by
    type
    '''
    def __init__(self):
        from datetime import datetime
        self.granted_to = u''
        self.vehicle_license_plate = u''
        self.expiration_date = datetime.now()
        self.processing_fee = u''
        self.receipt_id = u''
        self.authorized_by = u''
        self.place_of_origin = u''


class ProcessingFee(object):
    paid = u'PAID'
    waived = u'WAIVED'


class Executives(object):
    ceo = u'CEO'
    chairman = u'CHAIRMAN'


AccessLetterType = enum(
    courtesy=1,
    temporary_workers=2,
    external_services=3,
    exit_letters=4,
    brokers=5,
    peseros=6,
    mobile_vendors=7)


class CourtesyLetter(AccessLetter):
    def __init__(self):
        self.letter_type = AccessLetterType.courtesy

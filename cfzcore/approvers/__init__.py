from collections import namedtuple


class ApplicationApprover(object):

    def approve_application(self, application):
        self.verify_investors_information(application)
        application.approve()

    def verify_investors_information(self, application):
        investors = application.investors
        if len(investors) == 0:
            raise NoInvestorsException("The company %s can not be approved without investors" % application.company.name)
        for investor in investors:
            if investor.background_check is None:
                error = InvestorBackgroundCheckError()
                error.add_error(investor)
                raise InvestorBackgroundCheckEmptyException("Investors must have background check before they are approved!", error)
            if investor.has_passed_background_check() is False:
                raise InvestorBackgroundCheckFailed("Investor background check failed", (investor.name()))

    def deny_application(self, application, reason_for_denial):
        application.deny(reason_for_denial)


class NoInvestorsException(Exception):
    def __init__(self, message):
        Exception.__init__(self, message)

    def str(self):
        return repr(self.message)


class InvestorBackgroundCheckError(object):

    def __init__(self):
        self.investors = []
        self.error = "Does not have a background check!"

    def add_error(self, investor):
        InvestorError = namedtuple("InvestorError", "name error")
        investor_error = InvestorError(name=investor.name(), error=self.error)
        self.investors.append(investor_error)


class InvestorBackgroundCheckEmptyException(Exception):
    def __init__(self, message, error):
        Exception.__init__(self, message)
        self.error = error

    def str(self):
        return repr(self.message)


class InvestorBackgroundCheckFailed(Exception):
    def __init__(self, message, error):
        Exception.__init__(self, message)
        self.error = error

    def str(self):
        return repr(self.message)

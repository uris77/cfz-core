from collections import namedtuple


PersonTuple = namedtuple("Person", "id first_name last_name")


class Person(object):
    '''Representation of a person.

    This should be mapped to a persistence layer and include the following
    attributes:
        first_name
        last_name
        email
        phone1
        phone2
    '''
    def __init__(self, first_name, last_name):
        self.__validate_names(first_name, last_name)
        self.first_name = first_name
        self.last_name = last_name

    def name(self):
        return self.first_name + " " + self.last_name

    def __validate_names(self, first_name, last_name):
        if last_name is None or len(last_name) < 1:
            raise InvalidNumberOfNameArgumentsException("No Last Name was provided!")
        if first_name is None or len(first_name) < 1:
            raise InvalidNumberOfNameArgumentsException("No First Name was provided!")

    def update_name(self, **kwargs):
        self.first_name = kwargs.get("first_name")
        self.last_name = kwargs.get("last_name")

    def change_name(self, first_name, last_name):
        self.__validate_names(first_name, last_name)
        self.first_name = first_name
        self.last_name = last_name

    @property
    def __tuple__(self):
        person_tuple = PersonTuple(id=getattr(self, "id", None),
                                   first_name=self.first_name,
                                   last_name=self.last_name)
        return person_tuple


class InvalidNumberOfNameArgumentsException(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

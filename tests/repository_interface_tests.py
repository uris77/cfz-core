import unittest
from nose.tools import eq_

from tests.repository import RepositoryDouble, ApplicationRepositoryDouble


class RepositoryInterfaceTests(object):
    def __init__(self, repository):
        self.repository = repository

    def test_implements_get_entity_by_id(self):
        eq_(True, callable(getattr(self.repository, "get")))

    def test_implements_persistable_entity(self):
        eq_(True, callable(getattr(self.repository, "persist")))


class ApplicationRepositoryInterfaceTests(object):
    def __init__(self, repository):
        self.repository = repository

    def test_implements_get_entity_by_id(self):
        eq_(True, callable(getattr(self.repository, "get")))

    def test_implements_persistable_entity(self):
        eq_(True, callable(getattr(self.repository, "persist")))

    def test_implements_get_investor(self):
        eq_(True, callable(getattr(self.repository, "get_investor")))


class RepositoryDoubleTests(unittest.TestCase, RepositoryInterfaceTests):
    def setUp(self):
        self.repository = RepositoryDouble()


class ApplicationRepositoryTests(unittest.TestCase, ApplicationRepositoryDouble):
    def setUp(self):
        self.repository = ApplicationRepositoryDouble

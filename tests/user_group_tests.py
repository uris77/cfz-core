import unittest
from nose.tools import eq_
from cfzcore.user_account import UserGroup


class UserGroupTests(unittest.TestCase):

    def test_should_create_a_user_group(self):
        user_group = UserGroup.create("name")
        eq_("name", user_group.group_name)

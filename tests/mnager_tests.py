import unittest
from nose.tools import eq_

from cfzcore.company import Director 
from cfzcore.person import Person

class ManagerTests(unittest.TestCase):

    def  test_manager_should_have_a_start_date(self):
        start_date = "31/01/2010"
        director = self._create_director("FirstName", "Last Name", start_date)
        eq_("31/01/2010", director.get_start_date())

    def _create_director(self, first_name, last_name, start_date):
        from datetime import datetime
        _start_date = datetime.strptime(start_date, "%d/%m/%Y")
        director = Director(Person("FirstName", "LastName"), _start_date)
        return director


import  unittest
from nose.tools import eq_, raises
from cfzcore.person import Person, InvalidNumberOfNameArgumentsException


class PersonTests(unittest.TestCase):

    def test_create_person(self):
        person = Person("First", "Last")
        eq_("First Last", person.name())

    @raises(InvalidNumberOfNameArgumentsException)
    def test_creating_person_without_last_name_throws_exception(self):
        Person("First", "")

    @raises(InvalidNumberOfNameArgumentsException)
    def test_should_not_create_person_without_first_name(self):
        Person("", "Last")

    def test_should_edit_person_name(self):
        person = Person("First", "Last")
        person.change_name("Second", "Last")
        eq_("Second Last", person.name())

    @raises(InvalidNumberOfNameArgumentsException)
    def test_should_raise_exception_if_a_name_is_missing(self):
        person = Person("First", "Last")
        person.change_name("", "Last")

    def test_update_name(self):
        person = Person("First", "Last")
        kwargs = dict(first_name="Second", last_name="Name")
        person.update_name(**kwargs)
        eq_("Second", person.first_name)
        eq_("Name", person.last_name)

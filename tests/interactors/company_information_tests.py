import unittest
from nose.tools import eq_
from mock import Mock

from cfzcore.company import Company, CompanyType, CompanyParams
from cfzcore.interactors.company import CompanyContactNumbers


class CompanyInformationTests(unittest.TestCase):
    def setUp(self):
        params = CompanyParams(name="Company", registration_number="1")
        self.company = Company(params)
        self.company_id = 1
        self.repository = Mock()
        self.repository.get.return_value = self.company

    def test_should_update_contact_information(self):
        company_contact_numbers = CompanyContactNumbers(self.company_id, self.repository)
        contact_numbers = dict(telephone_number="1234567", fax_number="98765",
                               cell_number="4321")
        company_contact_numbers.update(**contact_numbers)
        eq_("1234567", self.company.telephone_number)
        eq_("4321", self.company.cell_number)
        eq_("98765", self.company.fax_number)
        self.repository.get.assert_called_once_with(self.company_id)
        self.repository.persist.assert_called_once_with(self.company)

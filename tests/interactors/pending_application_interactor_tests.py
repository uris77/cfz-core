import unittest
from nose.tools import eq_
from mock import Mock
from cfzcore.interactors.application_builder import PendingApplicationInteractor

class PendingApplicationInteractorTests(unittest.TestCase):
    def setUp(self):
        self.repository = Mock()
        self.pending_application_interactor = PendingApplicationInteractor(self.repository)

    def test_should_retrieve_all_pending_applications(self):
        self.pending_application_interactor.all_applications()
        self.repository.all_with_status_pending.assert_called_with()


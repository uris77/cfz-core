import unittest
from nose.tools import eq_
from mock import Mock

from collections import namedtuple
from tests.repository import ApplicationRepositoryDouble
from cfzcore.application import ApplicationInvestor
from cfzcore.interactors.application import ApplicationInvestorForm
from cfzcore.interactors.application_builder  import ApplicationFormBuilder


class ApplicationInvestorFormTests(unittest.TestCase):

    def setUp(self):
        del ApplicationRepositoryDouble.entities[:]
        ApplicationRepositoryDouble.id = 1
        self.application = None
        self.application = self.__create_application__()
        self.application_id = self.application.id

    def tearDown(self):
        del self.application.investors[:]

    def test_investor_is_added(self):
        investor = self.__add_investor__()
        eq_(True, isinstance(investor, ApplicationInvestor))
        eq_(1, len(ApplicationRepositoryDouble.entities))

    def test_should_retrieve_investors(self):
        self.__add_investor__()
        investor_form = ApplicationInvestorForm(ApplicationRepositoryDouble())
        investors = investor_form.get_all_investors_for(self.application_id)
        eq_(1, len(ApplicationRepositoryDouble.entities))
        eq_(1, len(investors))

    def test_should_remove_an_investor(self):
        self.__add_investor__()
        investor_form = ApplicationInvestorForm(ApplicationRepositoryDouble())
        investor_id = 1
        investor_form.remove_investor_from(self.application_id, investor_id)
        investors = investor_form.get_all_investors_for(self.application_id)
        eq_(0, len(investors))

    def test_add_background_test_to_investor(self):
        self.__add_investor__()
        investor_form = ApplicationInvestorForm(ApplicationRepositoryDouble())
        request_model = dict(investor_id=1, background_check="P")
        investor_form.add_background_check(**request_model)
        investor = self.application.investors[0]
        eq_('P', investor.background_check)

    def __add_investor__(self):
        Investor = namedtuple("Investor", "first_name last_name percent_owned")
        investor = Investor(first_name="First", last_name="Last",
                            percent_owned=50)
        investor_form = ApplicationInvestorForm(ApplicationRepositoryDouble())
        _investor = investor_form.add_investor_to(self.application_id, investor)
        return _investor

    def __create_application__(self):
        params = {'name': 'Company',
                  'company_type': 'Import/Export',
                  'registration_number': '1',
                  'first_name': 'First',
                  'last_name': 'Manager',
                  'submission_date': '30/01/2012'}
        builder = ApplicationFormBuilder(Mock())
        application = builder.initialize_application_form(**params)
        repository = ApplicationRepositoryDouble()
        repository.persist(application)
        return application

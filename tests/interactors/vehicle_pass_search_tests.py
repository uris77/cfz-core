import unittest
from mock import Mock

from cfzcore.interactors.vehicle_pass import search_for_vehicle_pass_by_owner


class SearchForVehiclePassTests(unittest.TestCase):
   
   def test_search_for_vehicle_pass_by_owner_name(self):
      gateway = Mock()
      owner_name = "Owner Name"
      search_for_vehicle_pass_by_owner(owner_name, gateway)
      gateway.search_by_owner.assert_called_once_with(owner_name)

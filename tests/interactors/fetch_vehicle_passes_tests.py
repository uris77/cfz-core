import unittest
from nose.tools import eq_

from tests.persistence.repository.doubles import (VehiclePassRepositoryDouble,
                                                  CompanyRepositoryDouble)

from cfzcore.interactors.vehicle_pass import (create_vehicle_pass,
                                              fetch_all_vehicle_passes,
                                              fetch_all_vehicle_passes_for_company)
from cfzcore.person import Person
from cfzcore.company import Company
from collections import namedtuple


class FetchVehiclePassesTests(unittest.TestCase):

    def setUp(self):
        self.company = self._create_company()
        self.repository = VehiclePassRepositoryDouble()

    def tearDown(self):
        VehiclePassRepositoryDouble.reset_pk()
        VehiclePassRepositoryDouble.reset_passes()

    def test_fetching_all_passes_should_return_100(self):
        for cnt in range(0, 100):
            self._create_vehicle_pass(self.company)
        all_passes = fetch_all_vehicle_passes(self.repository)
        eq_(100, len(all_passes))

    def test_fetching_all_passes_should_return_10(self):
        for cnt in range(0, 10):
            self._create_vehicle_pass(self.company)
        all_passes = fetch_all_vehicle_passes(self.repository)
        eq_(10, len(all_passes))

    def test_fetching_all_passes_should_return_1000(self):
        for cnt in range(0, 1000):
            self._create_vehicle_pass(self.company)
        all_passes = fetch_all_vehicle_passes(self.repository)
        eq_(1000, len(all_passes))

    def test_fetch_all_passes_for_a_company_with_10_records(self):
        company1 = self._create_company("Company 1")
        company2 = self._create_company("Company 2")
        for cnt in range(0, 100):
            self._create_vehicle_pass(company1)
        for cnt in range(0, 10):
            self._create_vehicle_pass(company2)
        company2_passes = fetch_all_vehicle_passes_for_company(self.repository,
                                                               company2.id)
        eq_(10, len(company2_passes))

    def test_fetch_all_passes_for_a_company_with_100_records(self):
        company1 = self._create_company("Company 1")
        company2 = self._create_company("Company 2")
        for cnt in range(0, 100):
            self._create_vehicle_pass(company1)
        for cnt in range(0, 10):
            self._create_vehicle_pass(company2)
        company1_passes = fetch_all_vehicle_passes_for_company(self.repository,
                                                               company1.id)
        eq_(100, len(company1_passes))

    def _create_vehicle_pass(self, company):
        pass_values = dict(company_id=company.id,
                           owner="Owner",
                           vehicle_year="2001",
                           vehicle_model="Mazda",
                           color="Blue",
                           license_plate="123123",
                           date_issued="21/09/2012",
                           sticker_number="00100",
                           receipt_number="0001",
                           month_expired="Dec")
        vehicle_pass = create_vehicle_pass(self.repository, **pass_values)
        return vehicle_pass

    def _create_company(self, name="Name"):
        manager = Person("Firstname", "Lastname")
        CompanyTuple = namedtuple('CompanyTuple', 'manager name\
                                   registration_number company_type')
        company_tuple = CompanyTuple(manager=manager, name=name,
                                     registration_number='12311',
                                     company_type='Import/Export')
        company = Company(company_tuple)
        repository = CompanyRepositoryDouble()
        company = repository.persist(company)
        return company

from nose.tools import eq_
import unittest
from mock import Mock
from datetime import datetime

from cfzcore.interactors.company import send_letter_of_acceptance


class SendLetterOfAcceptanceTests(unittest.TestCase):

    def test_should_send_letter_of_acceptance(self):
        company = Mock()
        company_id = 1
        repository = Mock()
        repository.get.return_value = company
        date_letter_sent = datetime.strptime("01/01/2012", "%d/%m/%Y")
        send_letter_of_acceptance(1, date_letter_sent, repository)
        repository.get.assert_called_once_with(company_id)
        repository.persist.assert_called_once_with(company)
        eq_(date_letter_sent, company.date_sent_letter_of_acceptance)

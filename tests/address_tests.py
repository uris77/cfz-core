import unittest
from nose.tools import eq_

from cfzcore.address import Address


class AddressTests(unittest.TestCase):
    def test_should_add_address_lines(self):
        address = Address()
        address.update("line1", "line2")
        eq_("line1", address.line1)
        eq_("line2", address.line2)


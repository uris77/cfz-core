import unittest
from nose.tools import eq_
from datetime import datetime

from mock import Mock, MagicMock

from cfzcore.person import Person
from cfzcore.company import Company, CompanyType, CompanyParams
from cfzcore.application import Application, Status


class ApplicationTests(unittest.TestCase):
    def setUp(self):
        self.manager = Person('First', 'Last')
        self.registration_number = "IN032443534"
        params = CompanyParams(name="Name", registration_number=self.registration_number)
        self.company = Company(params)
        self.application_values = dict(company=self.company,
                                       registration_number=self.registration_number,
                                       manager=self.manager,
                                       submission_date="30/01/2012")

        self.investor1 = Person("Investor1", "Last")
        self.investor2 = Person("Investor1", "Last")
        self.application = Application(**self.application_values)

    def test_should_create_application(self):
        eq_(self.manager, self.application.manager)
        eq_(self.company, self.application.company)
        eq_("30/Jan/2012", self.application.get_submission_date())

    def test_should_assign_investors(self):
        self.application.remove_all_investors()
        self.application.assign_investor(self.investor1, 50)
        eq_(1, len(self.application.investors))

    def test_should_remove_all_investors(self):
        self.application.assign_investor(self.investor1, 50)
        self.application.remove_all_investors()
        eq_(0, len(self.application.investors))

    def test_add_place_of_origin(self):
        self.application.set_place_of_origin("Miami", "USA")
        eq_("Miami, USA", self.application.place_of_origin())

    def test_should_add_submission_date(self):
        date_str = "01/12/2000"
        self.application.set_submission_date(date_str)
        eq_("01/Dec/2000", self.application.get_submission_date())

    def test_should_update_place_of_origin(self):
        params = {'state': 'Cayo', 'country': 'Belize'}
        self.application.update_place_of_origin(**params)
        eq_('Cayo', self.application.state)
        eq_('Belize', self.application.country)

    def test_should_return_false_if_place_of_origin_is_not_present(self):
        eq_(False, self.application.is_place_of_origin_present())

    def test_should_return_true_if_place_of_origin_is_present(self):
        self.application.update_place_of_origin(**dict(state="Cayo", country="Belize"))
        eq_(True, self.application.is_place_of_origin_present())

    def test_should_return_total_number_of_investors(self):
        self.application.remove_all_investors()
        self.application.assign_investor(self.investor1, 50)
        self.application.assign_investor(self.investor2, 50)
        eq_(2, self.application.total_number_of_investors())

    def test_should_get_formatted_created_date(self):
        self.application.created_date = datetime.strptime("01/12/2012", "%d/%m/%Y")
        eq_('01/Dec/2012', self.application.get_created_date())

    def test_should_set_reason_for_denial_when_denying_appplication(self):
        self.application.deny("Reason for denial")
        eq_('Reason for denial', self.application.reason_for_denial)

    def test_application_status_should_be_denied_when_application_is_denied(self):
        self.application.deny("Reason for denial")
        eq_(Status.DENIED, self.application.status)

    def test_approve_application(self):
        self.application.approve()
        eq_('Approved', self.application.status)

    def test_should_initialize_company_information_when_application_is_approved(self):
        self.application.investors = self.create_investors()
        self.application.company.assign_shareholder = MagicMock()
        self.application.approve()
        eq_("Approved", self.application.status)
        eq_(True, self.application.company.active)

    def create_investors(self):
        investor1 = Mock()
        investor1.person = Mock()
        investor1.percent_owned = 50
        investor1.background_check = Mock()
        investor2 = Mock()
        investor2.percent_owned = 50
        investor2.background_check = Mock()
        return [investor1, investor2]

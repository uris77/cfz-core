import unittest
from nose.tools import eq_
from cfzcore.company import (Company, CompanyParams, CompanyType)
from cfzcore.person import Person
from cfzcore.shareholder import Shareholder


class ShareholderTests(unittest.TestCase):

    def setUp(self):
        self.__create_shareholder()

    def test_should_create_a_shareholder(self):
        eq_(50, self.shareholder.shares)
        eq_("Clear", self.shareholder.background_check)

    def __create_shareholder(self):
        params = CompanyParams(name="Company",
                               registration_number='12345')
                               #company_type=CompanyType.IMPORT_EXPORT)
        company = Company(params)
        shareholder_params = dict(first_name="First", last_name="Shareholder",
                                  shares=50, background_check="Clear", company=company)
        self.shareholder = Shareholder(**shareholder_params)

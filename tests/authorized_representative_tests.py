import unittest
from nose.tools import eq_, raises
from mock import Mock

from cfzcore.company import (Company,
                             RepresentationType,
                             AuthorizedRepresentative)

from cfzcore.company import (IllegalPersonNameException,
                             NullCompanyException,
                             NullRepresentationTypeException)

class CreateAuthorizedRepresentativeTests(unittest.TestCase):
    def test_it_should_have_a_person(self):
        company = Mock()
        args = dict(company=company,
                    first_name=u"John",
                    last_name=u"Doe",
                    representation_type=Mock())
        representative = AuthorizedRepresentative.create(**args)
        self.assertIsNotNone(representative.person)
        eq_('John Doe', representative.person.name())

    @raises(IllegalPersonNameException)
    def test_it_should_raise_exception_if_illegal_person_name(self):
        company = Mock()
        args = dict(company=company,representation_type=Mock())
        representative = AuthorizedRepresentative.create(**args)

    @raises(NullCompanyException)
    def test_it_should_raise_exception_if_no_company_provided(self):
        args = dict(first_name=u"John",
                    last_name=u"Doe",
                    representation_type = Mock())
        AuthorizedRepresentative.create(**args)

    @raises(NullRepresentationTypeException)
    def test_it_should_raise_exception_if_representation_type_is_null(self):
        args = dict(company=Mock(),
                    first_name=u"John",
                    last_name=u"Doe")
        AuthorizedRepresentative.create(**args)

    def test_it_should_assign_a_signature(self):
        args = dict(company=Mock(),
                    first_name=u"John",
                    last_name=u"Doe",
                    representation_type=Mock())
        representative = AuthorizedRepresentative.create(**args)
        representative.signature = "/path/to/signature"
        eq_(representative.signature, "/path/to/signature")


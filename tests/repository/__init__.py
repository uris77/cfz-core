class RepositoryDouble(object):

    def get(self, id):
        pass

    def persist(self, entity):
        pass


class ApplicationRepositoryDouble(object):
    id = 1
    entities = []

    def get(self, id):
        for entity in ApplicationRepositoryDouble.entities:
            return entity

    def persist(self, entity):
        if hasattr(entity, 'id') is False:
            entity.id = ApplicationRepositoryDouble.id
            ApplicationRepositoryDouble.id += 1
            ApplicationRepositoryDouble.entities.append(entity)

    def all(self):
        return ApplicationRepositoryDouble.entities

    def get_investor(self, investor_id):
        return ApplicationRepositoryDouble.entities[0].investors[0]

from setuptools import setup, find_packages
import sys
import os


version = '0.1'

setup(name='cfz-core',
      version=version,
      description="Core Application for CFZ Company Registries",
      long_description="""\
Core Application for CFZ that keeps track of application submissions, reports and requests by Companies.""",
      classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
      keywords='cfz, application, request',
      author='Roberto Guerra',
      author_email='uris77@gmail.com',
      url='',
      license='Apache',
      packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
      include_package_data=True,
      zip_safe=False,
      test_suite='nose.collector',
      testpkgs=['nose',
                'coverage'],
      install_requires=[
	'nose', 'coverage', 'mock', 'cryptacular'
          # -*- Extra requirements: -*-
      ],
      entry_points="""
      # -*- Entry points: -*-
      """,
      )
